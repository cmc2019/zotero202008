* master

** 0


|    |                            | branches |
|    |                            |          |
| 10 |                            |          |
|    |                            |          |
| 20 |                            |          |
|    |                            |          |
| 30 | zotero Dockerfile          | zotero   |
|    |                            |          |
| 32 | 30 [2020-07-30 Thu 10:00]  |          |
|    |                            |          |
| 60 | zotero-chromium Dockerfile |          |
|    |                            |          |


** 10

** 30


| zotero202007 |
| master       |
| 48           |


#+HEADER: :tangle Dockerfile 
#+HEADERS: :results silent
#+BEGIN_SRC sh

FROM debian:stable-slim AS download

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    curl \
    bzip2 \
    && t=https://www.zotero.org/download/ && \
    v=$( \
    curl -s $t -o - | \
    grep linux-x86_64 | \
    sed 's/.*linux-x86_64\":\"\([0-9.]*\)\".*/\1/' ) \
    && u="https://www.zotero.org/download/client/dl?channel=release&platform=linux-x86_64&version=" \
    && d=/tmp/zotero.tar.bz2 \
    && curl -L $u$v -o $d \
    && tar jxvf $d -C /opt \
    && rm $d


FROM download AS zotero

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    libgtk-3-0 \
    libx11-xcb1 \
    libdbus-glib-1-2 \
    libxt6

#+END_SRC


** 32

|                               |                        |
| build as latest at docker hub | [2020-07-30 Thu 10:01] |
|                               |                        |
| docker run                    | [2020-07-30 Thu 10:01] |
|                               |                        |
| zotero version                | 5.0.89                 |
|                               |                        |
| build as zotero at docker hub | [2020-07-30 Thu 10:04] |
|                               |                        |
|                               |                        |


** 60


#+HEADERS: :tangle Dockerfile 
#+HEADERS: :results silent
#+BEGIN_SRC sh


#  FROM debian:stable-slim AS download
   FROM cmch/chromium202008:stable-slim AS download


ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    curl \
    bzip2 \
    && t=https://www.zotero.org/download/ && \
    v=$( \
    curl -s $t -o - | \
    grep linux-x86_64 | \
    sed 's/.*linux-x86_64\":\"\([0-9.]*\)\".*/\1/' ) \
    && u="https://www.zotero.org/download/client/dl?channel=release&platform=linux-x86_64&version=" \
    && d=/tmp/zotero.tar.bz2 \
    && curl -L $u$v -o $d \
    && tar jxvf $d -C /opt \
    && rm $d


FROM download AS zotero-chromium

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    libdbus-glib-1-2 \


#+END_SRC



* git branching model

** 0


| 10 | master |
|    |        |
| 20 | debian |
|    |        |
| 30 |        |


** 10


| master          |
|                 |
| zotero          |
|                 |
| zotero-chromium |


** 20

|               |         |          | 202007 - 202107   |               chromium |
|               |         |          |                   | [2020-07-29 Wed 10:16] |
|               |         |          |                   |                        |
|               |         |          |                   |                        |
| wheezy-slim   | 7-slim  |          | obsolete stable   |                        |
|               |         |          |                   |                        |
| jessie-slim   | 8-slim  | 20150425 | oldoldstable-slim |                     57 |
|               |         |          |                   |                        |
| stretch-slim  | 9-slim  | 20170617 | oldstable-slim    |                     73 |
|               |         |          |                   |                        |
| buster-slim   | 10-slim | 20190706 | stable-slim       |                     83 |
|               |         |          |                   |                        |
| bullseye-slim | 11-slim | 202106 ? | testing-slim      |                     83 |
|               |         |          |                   |                        |
| sid-slim      |         |          | unstable-slim     |                     83 |
|               |         |          |                   |                        |


https://en.wikipedia.org/wiki/Debian_version_history


https://hub.docker.com/_/debian?tab=description&page=1&ordering=-name


https://www.debian.org/releases/


https://packages.debian.org/search?keywords=chromium&searchon=names&suite=all&section=all
